package Problem4;

import java.util.ArrayList;

public class Generic<T> {
    private T type;
    private ArrayList<T> list;
    private String name;

    public Generic(T type, ArrayList<T> list, String name){
        this.type = type;
        this.list = list;
        this.name = name;
    }

    //Print the contents of this object to the console
    public void print(){
        System.out.println(this.name);
        try{
            for(T item : this.list){
                System.out.println(item);
            }
        }catch(NullPointerException e){
            //Do nothing. Nothing exists in the ArrayList as the ArrayList is not initalized yet.
        }

    }

    //Creates a new instance
    public static <T> Generic<T> createInstance(String name, T type){
        return new Generic<>(type, null, name);
    }
}

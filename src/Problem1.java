import java.util.ArrayList;
import java.util.List;

public class Problem1 {


    public static void main(String[] args){ //This exists to just quickly and easily verify the functionality of these methods
        taskOne(9999,2);

        int[][] two = {{0,1,2,3},{4,5,6,7},{8,9,10,11},{12,13,14,15}};
        printTaskTwo(two);
        int[][] rotated = taskTwo(two);
        System.out.println("");
        printTaskTwo(rotated);
        //Add a bunch of words to an ArrayList to demonstrate how Task 3 works
        ArrayList<String> words = new ArrayList<String>();
        words.add("Hello");
        words.add("Nope");
        words.add("Hell");
        words.add("what");
        words.add("Hel");
        System.out.println("Found:" + taskThree(words, "Hel", 0, 0));
    }

    //The names of these methods aren't particularly useful names, but they are self-documenting as to what task they solve.

    public static void taskOne(int x, int y){
        x = x + y;
        y = x - y;
        x  = x - y;
        System.out.println("X: " + x + "\nY: " + y);
    }
    //I hate this task. There doesn't seem to be an easy way to do this
    //without embedded loops...
    public static int[][] taskTwo(int[][] square){
        int lh = square.length; //Get the length of the inputted array
        //Run through each square one by one
        for (int x = 0; x < lh/2; x++) {
            for (int y = x; y < lh-x-1; y++) {
                int temp = square[x][y]; //Hold the current value
                square[x][y] = square[lh-1-y][x]; //Swap values to the right from the top
                square[lh-1-y][x] = square[lh -1-x][lh-1-y]; //Swap values from bottom to the left
                square[lh-1-x][lh-1-y] = square[y][lh-1-x]; //Move values from right to the bottom
                square[y][lh-1-x] = temp;  //Move temp to the right
            }
        }
        return square;
    }

    //This method exists just to print out the returned array from Task #2
    public static void printTaskTwo(int[][] input){
        for(int i = 0; i < input.length; i++){
            //print the first line...
            int[] row = input[i];
            for(int k = 0; k < row.length; k++){
                System.out.print(row[k] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int taskThree(List<String> list, String word, int index, int found){

        if(index < (list.size())){
            if(list.get(index).contains(word)){
                found++;
            }
            index++;
            return taskThree(list,word,index,found);
        }
        return found;

    }
}


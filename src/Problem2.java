import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
* Write a method that takes a Collection of Strings and returns a List of chars. Use only one
* stream, its associated methods, and no other java code to complete the follow requirements.
* Remove all strings containing the characters ‘!’ and ‘&’. Map the remaining strings to their chars.
* Remove chars if their ASCII values are divisible by 10. Sort all the remaining chars in
* descending order. Return the chars in a list.
 */


public class Problem2 {
    public static void main(String[] args){
        Collection<String> coll = new ArrayList<>();
        coll.add("!");
        coll.add("&");
        coll.add("Hello");
        coll.add("What");
        List<IntStream> list = problem2(coll);
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i).average());
        }
        System.out.println(list.toString());
    }

    public static List<IntStream> problem2(Collection<String> collection){
        List<IntStream> list;
        Stream<IntStream> stream = collection.stream().filter(str->!(str.contains("!") || str.contains("&")))
                .map(String::chars);
        list = stream.collect(Collectors.toList());
        return list;
    }
}

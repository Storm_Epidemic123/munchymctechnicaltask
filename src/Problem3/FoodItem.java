package Problem3;

public interface FoodItem {
    String getName();
    int getCalories();
    double getAmount();
    void setAmount(double newAmount);
    void updateConsumed(double consumed);
    double getConsumed();
    double getMade();
}

package Problem3;

import java.util.Scanner;

//This class is very ugly, and has a lot of print statements.
public class Driver {
    public static Cafeteria cafe = new Cafeteria();
    public static void main(String[] args){

        System.out.println("Welcome to the Cafeteria Program! Select one of the following options: \n" +
                "'A' -> Add a food to the cafeteria \n" +
                "'E' -> Update the consumed amount of a previously entered food \n" +
                "'-' -> Remove a food from the Cafeteria \n" +
                "'R' -> Print the end-of-day report for the cafeteria \n" +
                "'C' -> Remove all foods from the cafeteria (Reset entries) \n" +
                "'X' -> Exit Program");

        Scanner input = new Scanner(System.in);
        boolean running = true;
        while(running){
            String selection = input.next();
            switch(selection){
                case "A":
                    handleAddFood(input);
                    System.out.println("Success!");
                    break;
                case "E":
                    handleUpdateConsumption(input);
                    System.out.println("Success!");
                    break;
                case "-":
                    handleRemoveFood(input);
                    System.out.println("Success!");
                    break;
                case "R":
                    cafe.printReport();
                    break;
                case "C":
                    cafe.clearFoods();
                    break;
                case "X":
                    running = false;
                    break;
            }
            if(!selection.equals("X")){
                System.out.println("Select another option.");
            }


        }



    }



    private static void handleAddFood(Scanner input){
        System.out.println("Enter the type of the food you wish to enter (vegetable, meat, or dairy):");
        String type = input.next();
        System.out.println("Enter the name of the food you wish to enter:");
        String name = input.next();
        System.out.println("Enter the calories of the food you wish to enter:");
        int cal = input.nextInt();
        System.out.println("Enter the amount of the food you wish to enter (As a decimal):");
        double amount = input.nextDouble();

        cafe.addFood(type, name, cal, amount);
    }

    private static void handleUpdateConsumption(Scanner input){
        System.out.println("Enter the name of the food you wish to update:");
        String name = input.next();
        System.out.println("Enter the amount of food that has been consumed (as a decimal):");
        double consumed = input.nextDouble();
        cafe.updateFoodConsumption(name,consumed);
    }

    private static void handleRemoveFood(Scanner input){
        System.out.println("Enter the name of the food you wish to remove:");
        String name = input.next();
        cafe.removeFood(name);
    }

}

package Problem3.Dairy;

import Problem3.FoodItem;

public class Dairy implements FoodItem {
    String name;
    int calories;
    double amount; //Amount available in the cafeteria
    double consumed = 0; //Default to 0
    double made; //Total amount of this food made

    public Dairy(String name, int calories, double amount){
        this.name = name;
        this.calories = calories;
        this.amount = amount;
        this.made = amount;
    }
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getCalories() {
        return this.calories;
    }

    @Override
    public double getAmount() {
        return this.amount;
    }

    @Override
    public void setAmount(double newAmount) {
        this.amount = newAmount;
    }

    @Override
    public void updateConsumed(double consumed) {
        this.consumed += consumed;
        this.amount -= consumed;
    }

    @Override
    public double getConsumed() {
        return this.consumed;
    }

    @Override
    public double getMade() {
        return this.made;
    }
}

package Problem3.Meat;

import Problem3.FoodItem;

public class Meat implements FoodItem {
    String name;
    int calories;
    double amount; //Amount available in the cafeteria
    double consumed = 0; //Default to 0
    double made = 0; //Total amount of this food made

    public Meat(String name, int calories, double amount){
        this.name = name;
        this.calories = calories;
        this.amount = amount;
        this.made = amount;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getCalories() {
        return calories;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public void setAmount(double newAmount) {
        this.amount = newAmount;
    }

    @Override
    public void updateConsumed(double consumed) {
        this.consumed += consumed;
        this.amount -= consumed;
    }

    @Override
    public double getConsumed() {
        return consumed;
    }

    @Override
    public double getMade() {
        return this.made;
    }
}

package Problem3;

import Problem3.Dairy.Dairy;
import Problem3.Meat.Meat;
import Problem3.Vegetable.Vegetable;
import java.util.ArrayList;


public class Cafeteria {
    //List of foods, there are no entries by default- but only 2 of a particular category can be entered.
    ArrayList<FoodItem> foods = new ArrayList<>();
    int vegCount = 0;
    int meatCount = 0;
    int dairyCount = 0;


    //Constructor
    public Cafeteria() {};

    //Add a food to the list of foods in the Cafeteria
    //We are assuming someone doesn't try to add more than 2 entries into a food group...
    public void addFood(String classifier, String name, int calories, double amount){
        switch(classifier.toLowerCase()){
            case "vegetable":
                this.foods.add(new Vegetable(name,calories,amount));
                vegCount++;
                break;
            case "meat":
                foods.add(new Meat(name,calories,amount));
                meatCount++;
                break;
            case "dairy":
                foods.add(new Dairy(name,calories,amount));
                dairyCount++;
                break;
        }
    }
    //Update the consumed amount for a particular food
    public void updateFoodConsumption(String name, double amountConsumed){
        for(int i = 0; i < this.foods.size(); i++){ //Find the food based on the name given
            if(this.foods.get(i).getName().equals(name)){
                this.foods.get(i).updateConsumed(amountConsumed); //Update the consumed amount of the food
            }
        }
    }
    //Print the end-of-day report for the Cafeteria
    public void printReport(){
        for(FoodItem food : this.foods){
            if(food instanceof Vegetable){
                System.out.println("| Group: Vegetable");
            }else if(food instanceof Meat){
                System.out.println("| Group: Meat");
            }else{
                System.out.println("| Group: Dairy");
            }
            System.out.println("| Name: " + food.getName() + "\n" +
                    "| Calories: " + food.getCalories() + "\n" +
                    "| Amount Made: " + food.getMade() + "\n" +
                    "| Consumed: " + food.getConsumed() + "\n" +
                    "| Amount Available: " + food.getAmount() + "\n");
        }
    }
    //Clear the foods previously entered into the program.
    public void clearFoods(){
        foods.clear();
    }

    //Remove a food based on that food's name
    public void removeFood(String name){
        for(FoodItem food : this.foods){ //This isn't the most efficent way to do this, but at most there are only 6 possible foods...
            if(food.getName().equals(name)){
                this.foods.remove(food); //Remove the food from the list
                if(food instanceof Vegetable){
                    vegCount--;
                }else if(food instanceof Meat){
                    meatCount--;
                }else if(food instanceof Dairy){
                    dairyCount--;
                }
            }
        }
    }
}

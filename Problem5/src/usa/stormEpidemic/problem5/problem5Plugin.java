package usa.stormEpidemic.problem5;


import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntity;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class problem5Plugin extends JavaPlugin {
    @Override
    public void onEnable(){
        //Fired when the server enables the plugin
        ConsoleCommandSender console = getServer().getConsoleSender();
        console.sendMessage("WORKING!!");
        ProtocolManager manager = ProtocolLibrary.getProtocolManager();
        manager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.getInstance().values()) {
            @Override
            public void onPacketSending(PacketEvent event) {
                PacketContainer packet = event.getPacket();
                console.sendMessage(event.getPlayer().getName()); //Print the name of the player who sent the packet...
            }
        });

    }
    @Override
    public void onDisable(){
        //Fired when the server stops and disables all plugins
    }
}
